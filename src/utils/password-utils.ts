import bcrypt = require('bcrypt');

export const hashPassword = async (password: string ) => {
    const salt = await bcrypt.genSalt();
    const passwordHash = await bcrypt.hash(password, salt);
    return passwordHash;
};

export const comparePassword = (password: string, encryptedPassword: string) => {
    return bcrypt.compare(password, encryptedPassword);
};