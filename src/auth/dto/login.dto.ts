import {IsString, MinLength} from 'class-validator';
export class LoginDto {
    readonly username: string;
    @MinLength(5)
    readonly password: string;
}