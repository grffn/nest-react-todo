import * as mongoose from 'mongoose';
import * as jwt from 'jsonwebtoken';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';

import { AuthService } from '../auth/auth.service';
import { UsersService } from '../users/users.service';
import { UserSchema } from '../users/schemas/user.schema';
import { User } from '../users/interfaces/user.interface';
import { JWT_SECRET_KEY } from '../constants';
import { AUTH_ERROR } from '../errors';

describe('AuthService', async () => {
    let authService: AuthService;
    let userModel: mongoose.Model<User>;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(global.__MONGO_URI__),
                MongooseModule.forFeature([{
                    name: 'User',
                    schema: UserSchema,
                }]),
            ],
            providers: [
                AuthService,
                UsersService,
            ],
        }).compile();
        authService = module.get<AuthService>(AuthService);
        userModel = module.get<mongoose.Model<User>>(getModelToken('User'));

        await userModel.remove({}).exec();
    });

    describe('signIn', () => {
        it('should return correct token with username', async () => {
            const userDto = {
                firstName: 'First',
                lastName: 'Last',
                password: '12345',
                username: 'first.last',
                email: 'first.last@example.com',
            };
            const user = new userModel(userDto);
            const createdUser = await user.save();
            const token = await authService.signIn({
                password: userDto.password,
                username: userDto.username,
            });
            const expectedToken = jwt.sign({
                id: createdUser.id,
                firstName: 'First',
                lastName: 'Last',
                username: 'first.last',
                email: 'first.last@example.com',
            }, JWT_SECRET_KEY, {
                expiresIn: 3600,
            });
            expect(token).toEqual(expectedToken);
        });
        it('should return correct token with email', async () => {
            const userDto = {
                firstName: 'First',
                lastName: 'Last',
                password: '12345',
                username: 'first.last',
                email: 'first.last@example.com',
            };
            const user = new userModel(userDto);
            const createdUser = await user.save();
            const token = await authService.signIn({
                password: userDto.password,
                username: userDto.email,
            });
            const expectedToken = jwt.sign({
                id: createdUser.id,
                firstName: 'First',
                lastName: 'Last',
                username: 'first.last',
                email: 'first.last@example.com',
            }, JWT_SECRET_KEY, {
                expiresIn: 3600,
            } );
            expect(token).toEqual(expectedToken);
        });
        it('should throw with wrong password', async () => {
            const userDto = {
                firstName: 'First',
                lastName: 'Last',
                password: '12345',
                username: 'first.last',
                email: 'first.last@example.com',
            };
            const user = new userModel(userDto);
            let authPromise = authService.signIn({
                password: '123456',
                username: userDto.username,
            });
            expect(authPromise).rejects.toThrowError(AUTH_ERROR);
            authPromise = authService.signIn({
                password: '1234',
                username: userDto.username,
            });
            expect(authPromise).rejects.toThrowError(AUTH_ERROR);
            authPromise = authService.signIn({
                password: '',
                username: userDto.username,
            });
            expect(authPromise).rejects.toThrowError(AUTH_ERROR);
        });
    });
});