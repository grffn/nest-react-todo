import { Module } from '@nestjs/common';
import { UsersModule } from 'users/users.module';
import { JwtStrategy } from './jwt.strategy';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';

@Module({
    imports: [UsersModule],
    providers: [JwtStrategy, AuthService],
    controllers: [AuthController],
})
export class AuthModule {}