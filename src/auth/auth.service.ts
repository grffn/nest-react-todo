import * as jwt from 'jsonwebtoken';

import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { JWT_SECRET_KEY } from '../constants';
import { User } from '../users/interfaces/user.interface';
import { LoginDto } from './dto/login.dto';
import { comparePassword } from '../utils/password-utils';
import { AUTH_ERROR } from '../errors';
import { UserDto } from 'users/dto/user.dto';

@Injectable()
export class AuthService {
    constructor(private readonly usersService: UsersService) { }

    async signIn(loginDto: LoginDto) {
        const user = await this.usersService.findUserByEmailOrUsername(loginDto.username);
        if (!user || !comparePassword(loginDto.password, user.password)) {
            throw new Error(AUTH_ERROR);
        }
        return this.createToken(user);
    }

    async createToken(user: User) {
        const userPayload: JwtPayload = {
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            username: user.username,
            email: user.email,
        };
        return jwt.sign(userPayload, JWT_SECRET_KEY, { expiresIn: 3600 });
    }

    async validateUser(payload: JwtPayload): Promise<UserDto> {
        return await this.usersService.findUser(payload.id);
    }

}