import { Controller, Body, Post, BadRequestException, HttpCode } from '@nestjs/common';
import { LoginDto } from './dto/login.dto';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';
import { AUTH_ERROR } from 'errors';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Post('login')
    @HttpCode(200)
    async login(@Body() loginDto: LoginDto) {
        try {
            const token = await this.authService.signIn(loginDto);
            return {
                token,
            };
        } catch (error) {
            if (error.message === AUTH_ERROR) {
                throw new BadRequestException('User with given username or password was not found');
            }
            throw error;
        }
    }
}