import { Matches } from 'class-validator';

export class UpdateUserDto {
    @Matches(/^\w(\w|\d|_)+$/)
    readonly username: string;
    @Matches(/^[A-Z][a-z]+$/)
    readonly firstName: string;
    @Matches(/^[A-Z][a-z-]+$/)
    readonly lastName: string;
}