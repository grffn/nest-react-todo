import { Matches, MinLength, IsEmail } from 'class-validator';

export class CreateUserDto {
    @Matches(/^\w(\w|\d|_)+$/)
    readonly username: string;
    @MinLength(5)
    readonly password: string;
    @Matches(/^[A-Z][a-z]+$/)
    readonly firstName: string;
    @Matches(/^[A-Z][a-z-]+$/)
    readonly lastName: string;
    @IsEmail()
    readonly email: string;
}