import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './interfaces/user.interface';
import { FilterUserDto } from './dto/filter-user.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { DUPLICATE_USER, USER_NOT_FOUND } from '../errors';
import { UserDto } from './dto/user.dto';

@Injectable()
export class UsersService {

  constructor(@InjectModel('User') private readonly userModel: Model<User>) {
  }

  async findAll(filter: FilterUserDto): Promise<UserDto[]> {
    const users = await this.userModel.find().exec();
    return users.map(user => new UserDto(user));
  }

  async findUser(id: string): Promise<UserDto> {
    const user = await this.userModel.findById(id).exec();
    if (!user) throw new Error(USER_NOT_FOUND);
    return new UserDto(user);
  }

  findUserByEmailOrUsername(emailOrUsername: string): Promise<User> {
    return this.userModel.find()
      .or([{ email: emailOrUsername }, { username: emailOrUsername }])
      .findOne()
      .exec();
  }

  async create(user: CreateUserDto): Promise<User> {
    const countDuplicates = await this.userModel
      .find()
      .or([{ email: user.email }, { username: user.username }]);
    if (countDuplicates.length > 0) {
      throw new Error(DUPLICATE_USER);
    }
    const createdUser = new this.userModel(user);
    return await createdUser.save();
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<UserDto> {
    const user = await this.userModel.findByIdAndUpdate(id, updateUserDto, {
      new: true,
    });
    if (!user) {
      throw new Error(USER_NOT_FOUND);
    }
    return new UserDto(user);
  }

  delete(id: string): Promise<void> {
    return this.userModel.deleteOne({ _id: id }).exec();
  }
}
