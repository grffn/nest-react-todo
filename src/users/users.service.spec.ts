import { Test } from '@nestjs/testing';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { UsersService } from './users.service';
import { UserSchema } from './schemas/user.schema';
import { comparePassword } from '../utils/password-utils';
import { UpdateUserDto } from './dto/update-user.dto';
import { DUPLICATE_USER, USER_NOT_FOUND, AUTH_ERROR } from '../errors';
import { User } from './interfaces/user.interface';

describe('UsersService', () => {
    let usersService: UsersService;
    let userModel: mongoose.Model<User>;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(global.__MONGO_URI__),
                MongooseModule.forFeature([{
                    name: 'User',
                    schema: UserSchema,
                }]),
            ],
            providers: [
                UsersService,
            ],
        }).compile();
        usersService = module.get<UsersService>(UsersService);
        userModel = module.get<mongoose.Model<User>>(getModelToken('User'));

        await userModel.remove({}).exec();
    });

    describe('findUserByEmailOrUsername', () => {
        it('should find user by email', async () => {
            const dto = {
                email: 'abcd.def@example.com',
                firstName: 'Abcd',
                lastName: 'Def',
                password: '12345',
                username: 'abcd.def',
            };
            const user = await usersService.create(dto);
            const foundUser = await usersService.findUserByEmailOrUsername(dto.email);
            expect(foundUser._id).toEqual(user._id);
        });

        it('should find user by username', async () => {
            const dto = {
                email: 'abcd.def@example.com',
                firstName: 'Abcd',
                lastName: 'Def',
                password: '12345',
                username: 'abcd.def',
            };
            const user = await usersService.create(dto);
            const foundUser = await usersService.findUserByEmailOrUsername(dto.username);
            expect(foundUser).toBeTruthy();
            expect(foundUser._id).toEqual(user._id);
        });

        it('should throw AUTH_ERROR if no user found', async () => {
            const userPromise = usersService.findUserByEmailOrUsername('abc.def@ex.com');
            expect(userPromise).rejects.toThrow(AUTH_ERROR);
        });
    });

    describe('create', () => {
        it('should create user', async () => {
            const dto = {
                email: 'abcd.def@example.com',
                firstName: 'Abcd',
                lastName: 'Def',
                password: '12345',
                username: 'abcd.def',
            };
            const user = await usersService.create(dto);
            expect(user).toBeTruthy();
            expect(user).toMatchObject({
                email: dto.email,
                firstName: dto.firstName,
                lastName: dto.lastName,
                username: dto.username,
            });
            expect(await comparePassword(dto.password, user.password)).toBe(true);
        });

        it('should not create user with existing username', async () => {
            const firstDto = {
                email: 'abcd.def@example.com',
                firstName: 'Abcd',
                lastName: 'Def',
                password: '12345',
                username: 'abcd.def',
            };
            const secondDto = {
                email: '123.456@example.com',
                firstName: 'Bcde',
                lastName: 'ghi',
                password: '12345',
                username: 'abcd.def',
            };
            const firstUser = await usersService.create(firstDto);
            expect(firstUser).toBeTruthy();
            const secondUserPromise = usersService.create(secondDto);
            expect(secondUserPromise).rejects.toThrow(DUPLICATE_USER);
        });

        it('should not create user with existing email', async () => {
            const firstDto = {
                email: 'abcd.def@example.com',
                firstName: 'Abcd',
                lastName: 'Def',
                password: '12345',
                username: 'abcd.def',
            };
            const secondDto = {
                email: 'abcd.def@example.com',
                firstName: 'Bcde',
                lastName: 'ghi',
                password: '12345',
                username: '123.456',
            };
            const firstUser = await usersService.create(firstDto);
            expect(firstUser).toBeTruthy();
            const secondUserPromise = usersService.create(secondDto);
            expect(secondUserPromise).rejects.toThrow(DUPLICATE_USER);
        });
    });

    describe('update', async () => {
        it('should update user', async () => {
            const createDto = {
                email: 'abcd.def@example.com',
                firstName: 'Abcd',
                lastName: 'Def',
                password: '12345',
                username: 'abcd.def',
            };
            const createdUser = await usersService.create(createDto);
            expect(createdUser).toBeTruthy();
            const updateDto: UpdateUserDto = {
                firstName: 'Qwer',
                lastName: 'Uio',
                username: 'qwer.uio',
            };
            const updatedUser = await usersService.update(createdUser._id, updateDto);
            expect(updatedUser).toBeTruthy();
            expect(updatedUser).toMatchObject(updateDto);
            expect(updatedUser.email).toEqual(createDto.email);
        });
        it('should not update not existing user', async () => {
            const updateDto: UpdateUserDto = {
                firstName: 'Qwer',
                lastName: 'Uio',
                username: 'qwer.uio',
            };
            const updatedUserPromise = usersService.update(new mongoose.mongo.ObjectId().toHexString(), updateDto);
            expect(updatedUserPromise).rejects.toThrow(USER_NOT_FOUND);
        });
    });
});