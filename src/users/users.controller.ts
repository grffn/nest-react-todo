import { Get, Controller, Put, Post, Body, Delete, Param, Query, HttpException, ForbiddenException, UseGuards, NotFoundException, InternalServerErrorException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { FilterUserDto } from './dto/filter-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { DUPLICATE_USER, USER_NOT_FOUND } from '../errors';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) { }

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    try {
      return await this.userService.create(createUserDto);
    } catch (error) {
      if (error === DUPLICATE_USER) {
        throw new ForbiddenException('Cannot create user with duplicate username or email');
      }
      throw error;
    }
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  async update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    try {
      return this.userService.update(id, updateUserDto);
    } catch (error) {
      if (error.message === USER_NOT_FOUND) {
        throw new NotFoundException('User was not found');
      }
      throw error;
    }
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async delete(@Param('id') id: string) {
    try {
      await this.userService.delete(id);
    } catch (error) {
      if (error.message === USER_NOT_FOUND) {
        throw new NotFoundException('User was not found');
      }
      throw error;
    }
  }

  @Get()
  findAll(@Query() filter: FilterUserDto) {
    return this.userService.findAll(filter);
  }

  @Get(':id')
  async findUser(@Param('id') id: string) {
    try {
      return await this.userService.findUser(id);
    } catch (error) {
      if (error.message === USER_NOT_FOUND) {
        throw new NotFoundException('User was not found');
      }
      throw error;
    }
  }
}
