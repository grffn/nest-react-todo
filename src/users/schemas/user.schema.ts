import * as mongoose from 'mongoose';
import bcrypt = require('bcrypt');
import { hashPassword } from '../../utils/password-utils';

export const UserSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    email: String,
    username: String,
    password: String,
}).pre('save', async function(next) {
    if (!this.isModified('password')) return next();
    try {
        const password = await hashPassword(this.get('password'));
        this.set('password', password);
        next();
    } catch (error) {
        next(error);
    }
});