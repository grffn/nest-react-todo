export const MONGO_URI = 'mongodb://nest-react-todo:nest-react-todo+123@ds121652.mlab.com:21652/nest-react-todo';

export const JWT_SECRET_KEY = 'JWT_SECRET_KEY';

export const JWT_EXPIRATION_TIME = 3600;