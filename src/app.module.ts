import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MONGO_URI } from './constants';
import { UsersModule } from 'users/users.module';
import { AuthModule } from 'auth/auth.module';

@Module({
    imports: [MongooseModule.forRoot(MONGO_URI), UsersModule, AuthModule],
    exports: [MongooseModule],
})
export class ApplicationModule {
}