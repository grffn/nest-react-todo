class ApiService {

    sendRequest = async (endPoint, method, data, token) => {
        if (!method) method = "GET"
        const body = method === "GET" ? null : (data && JSON.stringify(data))
        const headers = {
            'Content-Type': 'application/json; charset=utf-8',
        }
        if (!!token) {
            headers['Authorization'] = 'Bearer ' + token
        }
        const response = await fetch(`/api/${endPoint}`, {
            method: method,
            headers: headers,
            body: body
        })
        if (response.ok) {
            const text = await response.text()
            try {
                return await JSON.parse(text)
            } catch (error) {
                return text
            }
        }
        const errorResponse = await response.json()
        throw new Error(errorResponse.message)
    }

    login = async (username, password) => {
        const response = await this.sendRequest('auth/login', 'POST', {
            username: username,
            password: password
        })
        return response.token
    }

    register = (data) => {
        return this.sendRequest('users', 'POST', data)
    }

    editUser = (userId, token, data) => {
        return this.sendRequest(`users/${userId}`, 'PUT', data, token)
    }

    deleteUser = (userId, token) => {
        return this.sendRequest(`users/${userId}`, 'DELETE', null, token)
    }

    getUsers = () => {
        return this.sendRequest('users', 'GET')
    }

    getUserById = (userId) => {
        return this.sendRequest(`users/${userId}`, 'GET')
    }
}

export default new ApiService()