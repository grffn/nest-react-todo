import React from "react"

const AuthContext = React.createContext()

export const withAuthContext = (Component) => {
    return (props) => (
        <AuthContext.Consumer>
            {({token, login, logout, user}) => <Component {...props} token={token} login={login} logout={logout} user={user} />}
        </AuthContext.Consumer>
    )
}

export const AuthContextProvider = AuthContext.Provider