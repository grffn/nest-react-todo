import React, { Component, Fragment } from 'react';
import "./Header.css"
import { Link } from 'react-router-dom';
import { withAuthContext } from '../AuthContext';
import Redirect from 'react-router-dom/Redirect';

class Header extends Component {
    constructor() {
        super()
        this.state = {
            redirect: false
        }
    }
    render = () => {
        if (this.state.redirect) {
            return <Redirect to="/"/>
        }
        return (
        <header className={this.props.className}>
            <div className="container">
                <h1 className="title"><Link to='/'>User App</Link></h1>
                <div className="user">
                    {!!this.props.token ?
                        <Fragment><Link to='/profile'>{`${this.props.user.firstName} ${this.props.user.lastName}`}</Link><a href="#" onClick={(e) => {
                            e.preventDefault()
                            this.props.logout()
                            this.setState({
                                redirect: false
                            })
                        }
                        }>Log out</a></Fragment> :
                        <Fragment><Link to='/sign-in'>Sign in</Link> <Link to='/register'>Register</Link></Fragment>
                    }
                </div>
            </div>
        </header>
        )
    }
}

const HeaderWithContext = withAuthContext(Header)

export { HeaderWithContext as Header }