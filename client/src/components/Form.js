import React, { Component } from 'react';
import './Form.css'

export class Form extends Component {
    submitForm = (e) => {
        e.preventDefault()
        this.props.onSubmit()
    }

    render = () => (
        <form className="form" onSubmit={this.submitForm}>
            {this.props.children}
            <button type="submit" >{this.props.formSubmitTitle}</button>
        </form>
    )
}