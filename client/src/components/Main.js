import React, { Component } from 'react';
import "./Main.css"
import { Switch } from 'react-router-dom';
import { Home } from '../pages/Home';
import { Login } from '../pages/Login';
import { Profile } from '../pages/Profile';
import { Register } from '../pages/Register';
import { EditProfile } from '../pages/EditProfile';
import { PropsRoute, PrivateRoute } from '../route-helpers';

export class Main extends Component {
    render = () => (
        <Switch>
            <PropsRoute exact path="/" component={Home} className={this.props.className} />
            <PropsRoute path="/sign-in" component={Login} className={this.props.className} />
            <PropsRoute path="/register" component={Register} className={this.props.className} />
            <PrivateRoute exact path="/profile" redirectTo="/" component={Profile} className={this.props.className} />
            <PrivateRoute to="/profile/edit" redirectTo="/" component={EditProfile} className={this.props.className} />
        </Switch>
    )
}