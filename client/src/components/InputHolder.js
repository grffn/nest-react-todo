import React, {Component} from 'react'
import "./InputHolder.css"

export class InputHolder extends Component {
    render = () => (
        <div className="input-holder">
            <p className="input-title">{this.props.inputTitle}</p>
            <input type={this.props.inputType} value={this.props.inputValue} onChange={this.props.onChange}/>
        </div>
    )
}