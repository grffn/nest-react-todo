import React, { Component } from "react"
import { InputHolder } from "../components/InputHolder";
import apiService from "../ApiService";
import { Form } from "../components/Form";
import Redirect from "../../node_modules/react-router-dom/Redirect";

export class Register extends Component {
    constructor() {
        super()
        this.state = {
            firstName: '',
            lastName: '',
            username: '',
            password: '',
            confirmPassword: '',
            email: '',
            redirect: false
        }
    }

    onUsernameChange = (e) => {
        this.setState({ username: e.target.value })
    }
    onEmailChange = (e) => {
        this.setState({ email: e.target.value })
    }
    onPasswordChange = (e) => {
        this.setState({ password: e.target.value })
    }
    onConfirmPasswordChange = (e) => {
        this.setState({ confirmPassword: e.target.value })
    }
    onFirstNameChange = (e) => {
        this.setState({ firstName: e.target.value })
    }
    onLastNameChange = (e) => {
        this.setState({ lastName: e.target.value })
    }

    register = async () => {
        try {
            if (this.state.password !== this.state.confirmPassword) {
                alert('Passwords do not match')
                return
            }
            await apiService.register({
                username: this.state.username,
                email: this.state.email,
                password: this.state.password,
                firstName: this.state.firstName,
                lastName: this.state.lastName
            })
            alert('You\'ve been registered')
            this.setState({
                redirect: true
            })
        } catch (error) {
            alert(error)
        }
    }
    render = () => {
        if (this.state.redirect) {
            return <Redirect to="/" />
        }
        return (
            <div className={this.props.className}>
                <Form onSubmit={this.register} formSubmitTitle="Register">

                    <InputHolder inputType="text" inputValue={this.state.username} inputTitle="User name" onChange={this.onUsernameChange} />
                    <InputHolder inputType="text" inputValue={this.state.email} inputTitle="Email" onChange={this.onEmailChange} />
                    <InputHolder inputType="password" inputValue={this.state.password} inputTitle="Password" onChange={this.onPasswordChange} />
                    <InputHolder inputType="password" inputValue={this.state.confirmPassword} inputTitle="Confirm password" onChange={this.onConfirmPasswordChange} />
                    <InputHolder inputType="text" inputValue={this.state.firstName} inputTitle="First name" onChange={this.onFirstNameChange} />
                    <InputHolder inputType="text" inputValue={this.state.lastName} inputTitle="Last name" onChange={this.onLastNameChange} />
                </Form>
            </div>
        )
    }

}