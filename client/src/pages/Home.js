import React, { Component } from "react"
import apiService from "../ApiService";

export class Home extends Component {
    constructor() {
        super()
        this.state = {
            users: []
        }
    }
    componentDidMount = async () => {
        const users = await apiService.getUsers()
        this.setState({ users: users })
    }
    render = () => (
        <div className={this.props.className}>
            <h1>Users</h1>
            <table>
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Full name</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.users.map(user => (
                        <tr key={user._id}>
                            <td>{user.username}</td>
                            <td>{`${user.firstName} ${user.lastName}`}</td>
                        </tr>
                    ))}
                </tbody>
            </table>

        </div>
    )
}