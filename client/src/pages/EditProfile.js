import React, { Component } from 'react'
import './EditProfile.css'
import { Form } from '../components/Form';
import apiService from '../ApiService';
import { InputHolder } from '../components/InputHolder';
import { Redirect } from 'react-router-dom';

export class EditProfile extends Component {
    constructor() {
        super()
        this.state = {
            profile: {
                username: '',
                firstName: '',
                lastName: '',
            },
            redirect: false
        }
    }
    componentDidMount() {
        console.log(this.props)
        this.setState({
            profile: {
                ...this.props.user
            }
        })
    }
    onUsernameChange = (e) => {
        this.setState({
            ...this.state,
            profile: {
                ...this.state.profile,
                username: e.target.value
            }
        })
    }
    onFirstNameChange = (e) => {
        this.setState({
            ...this.state,
            profile: {
                ...this.state.profile,
                firstName: e.target.value
            }
        })
    }
    onLastNameChange = (e) => {
        this.setState({
            ...this.state,
            profile: {
                ...this.state.profile,
                lastName: e.target.value
            }
        })
    }
    editProfile = async () => {
        await apiService.editUser(this.props.user.id, this.props.token, this.state.profile)
        this.setState({
            redirect: true
        })
    }
    render = () => {
        if (this.state.redirect)
            return <Redirect to="/profile" />
        const profile = this.state.profile
        return (
            <Form formSubmitTitle="Save" onSubmit={this.editProfile}>
                <InputHolder inputType="text" inputValue={profile.username} inputTitle="User name" onChange={this.onUsernameChange} />
                <InputHolder inputType="text" inputValue={profile.firstName} inputTitle="First name" onChange={this.onFirstNameChange} />
                <InputHolder inputType="text" inputValue={profile.lastName} inputTitle="Last name" onChange={this.onLastNameChange} />
            </Form>
        )
    }
}