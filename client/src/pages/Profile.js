import React, { Component } from "react"
import { Redirect, Link } from "react-router-dom";
import apiService from "../ApiService";
import { withAuthContext } from "../AuthContext";

import "./Profile.css"

class Profile extends Component {
    constructor() {
        super()
        this.state = {
            profile: {
                username: '',
                email: '',
                firstName: '',
                lastName: '',
            },
            redirect: false
        }
    }
    componentDidMount = async () => {
        try {
        const user = await apiService.getUserById(this.props.user.id)
        this.setState({
            profile: user
        })
    } catch (error) {
        this.setState({
            redirect: true
        })
    }
    }
    deleteProfile = async () => {
        await apiService.deleteUser(this.props.user.id, this.props.token)
        this.props.logout()
        this.setState({
            redirect: true
        })
    }
    render = () => {
        if (this.state.redirect) {
            return <Redirect to="/" />
        }
        const profile = this.state.profile
        return (
            <div className={this.props.className}>
                <div>Username: {profile.username}</div>
                <div>Email: {profile.email}</div>
                <div>First name: {profile.firstName}</div>
                <div>Last name: {profile.lastName}</div>
                <div className="actions">
                    <Link to={{ pathname: "/profile/edit", state: {profile: profile} }}>Edit</Link>
                    <button type="button" onClick={this.deleteProfile}>Delete</button>
                </div>
            </div>
        )
    }
}

const ProfileWithContext = withAuthContext(Profile)

export { ProfileWithContext as Profile }