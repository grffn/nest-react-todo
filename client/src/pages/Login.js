import React, { Component } from "react"
import { withAuthContext } from "../AuthContext";
import apiService from "../ApiService";
import { InputHolder } from "../components/InputHolder";
import { Form } from "../components/Form";
import { Redirect } from "../../node_modules/react-router-dom";

class Login extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: '',
            redirect: false
        }
    }
    onUsernameChange = (e) => {
        this.setState({ username: e.target.value })
    }
    onPasswordChange = (e) => {
        this.setState({ password: e.target.value })
    }
    login = async (e) => {
        try {
            const token = await apiService.login(this.state.username, this.state.password)
            await this.props.login(token)
            this.setState({
                redirect: true
            })
        } catch (error) {
            alert(error)
        }
    }
    render = () => {
        if (this.state.redirect) {
            return <Redirect to="/" />
        }
        return <div className={this.props.className}>
            <Form onSubmit={this.login} formSubmitTitle="Sign in">
                <InputHolder inputType="text" inputValue={this.state.username} inputTitle="User name" onChange={this.onUsernameChange} />
                <InputHolder inputType="password" inputValue={this.state.password} inputTitle="Password" onChange={this.onPasswordChange} />
            </Form>
        </div>
    }
}
const LoginWithContext = withAuthContext(Login)
export { LoginWithContext as Login }