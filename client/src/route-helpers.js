import React from 'react'
import { Route, Redirect } from 'react-router-dom';
import { withAuthContext } from './AuthContext';

export const renderMergedProps = (component, ...rest) => {
    const finalProps = Object.assign({}, ...rest);
    return (
        React.createElement(component, finalProps)
    );
}

export const PropsRoute = ({ component, ...rest }) => {
    return (
        <Route {...rest} render={routeProps => {
            return renderMergedProps(component, routeProps, rest);
        }} />
    );
}

const PrivateRoute = ({ component, redirectTo, ...rest }) => {
    return (
        <Route {...rest} render={routeProps => {
            return !!rest.token ? (
                renderMergedProps(component, routeProps, rest)
            ) : (
                    <Redirect to={{

                        pathname: redirectTo,
                        state: { from: routeProps.location }
                    }} />
                );
        }} />
    );
};

const PrivateRouteWithContext = withAuthContext(PrivateRoute)

export { PrivateRouteWithContext as PrivateRoute }