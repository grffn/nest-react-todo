import { decode } from 'jsonwebtoken'

const TOKEN_KEY = 'token'

class AuthService {
    token = null
    user = {}

    constructor() {
        const token = localStorage.getItem(TOKEN_KEY)
        if (!!token)
            this.setToken(token)
    }

    get isAuthorized() {
        return this.token === null
    }

    setToken(token) {
        this.token = token
        this.user = decode(token)
    }

    login = (token) => {
        this.setToken(token)
        localStorage.setItem(TOKEN_KEY, token)
    }

    logout = (token) => {
        this.token = null
        this.user = {}
        localStorage.removeItem(TOKEN_KEY)
    }
}

export default new AuthService()