import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom'
import './App.css';
import { Main } from './components/Main';
import { AuthContextProvider } from "./AuthContext"
import authService from "./AuthService"
import { Header } from './components/Header';


class App extends Component {
  render() {
    return (
      <div className="app">
        <Header className="header">
        </Header>
        <main className="main">
          <div className="container">
            <Main>
            </Main>
          </div>
        </main>
      </div>
    );
  }
}

export default () => (
  <AuthContextProvider value={authService}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </AuthContextProvider>
)
