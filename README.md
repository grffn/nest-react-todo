# nest-react-todo

## Description

App with backend on Nest.js and frontend on React.

Backend is located on `/api` path. App includes the following endpoints:
* `POST /api/auth/login` - Sign in user
* `GET /api/users` - All registered users
* `GET /api/users/:id` - Get user by id
* `PUT /api/users/:id` - Update registered user
* `POST /api/users/:id` - Register a user
* `DELETE /api/users/:id` - Delete registered user

## Installation

```bash
$ npm install
$ cd client && npm install 
```

## Running the app

You should use the following command to run the app in dev mode 
```bash
# development
$ npm run dev
```
It will run backend and frontend apps simultaneously and open the app in browser. App will also be updated on changes

## Test

You should use the following command to run tests:
```bash
$ npm run test
```
It will run tests on backend. Test environment includes in-memory MongoDB instance
